module.exports = {
  module: {
    rules: [
      {
        test: '\.pug$',
        loader: 'pug-plain-loader'
      },
      {
        test: '\.scss$',
        loader: ['style', 'css', 'sass']
      }
    ]
  }
}
